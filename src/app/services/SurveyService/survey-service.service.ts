import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SessionService } from '../session/session.service';

@Injectable({
  providedIn: 'root'
})
export class SurveyServiceService {
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.session.get().token
    })
  }

  constructor(private http: HttpClient, private session: SessionService) { }

  getSurveys(): Promise<any> {
    return this.http.get(`${environment.apiUrl}/v1/api/surveys`, this.httpOptions).toPromise();
  }

  // getSurveyById(surveyId): Promise<any> {
  //   return this.http.get(`${environment.apiUrl}/v1/api/surveys/${surveyId}`, this.httpOptions).toPromise();
  // }

  
  getSurveyById(): Promise<any> {
    return this.http.get(`${environment.apiUrl}/v1/api/surveys/1`, this.httpOptions).toPromise();
  }


}
