import { Component, OnInit } from '@angular/core';
import { SurveyServiceService } from 'src/app/services/SurveyService/survey-service.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-survey-detail',
  templateUrl: './survey-detail.component.html',
  styleUrls: ['./survey-detail.component.css']
})
export class SurveyDetailComponent implements OnInit {
  surveyForm: FormGroup = new FormGroup({
    surveyid: new FormControl('', [Validators.required])
  });

  survey: any = {}

  constructor(private service: SurveyServiceService) { }

  ngOnInit(): void {
  }
  get surveyid() {
    return this.surveyForm.get('surveyid');
  }

  async getOneSurvey(id) {

    try {
      const result: any = await this.service.getSurveyById();
      console.log(result);
      this.survey = result.data
      
    } catch (e) {
      console.error(e);
      
    }
  }

}
