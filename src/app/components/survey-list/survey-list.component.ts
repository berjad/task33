import { Component, OnInit } from '@angular/core';
import { SurveyServiceService } from 'src/app/services/SurveyService/survey-service.service';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {

  surveys = []

  constructor(private service: SurveyServiceService) { }

  async ngOnInit() {
 

    try {
      const result: any = await this.service.getSurveys();
      console.log('Result:', result);
      this.surveys = result.data
      
    } catch (e) {
      console.error('Error:', e);
      
    }
  }
}
