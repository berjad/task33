# Task 33 - Angular Routing
Convert your Dashboard component to a Lazy Loaded Component

Add 3 more components to your application

- SurveyList
- SurveyListItem
- SurveyDetail

Add a two new routes: (Both Lazy Loaded)

- /surveys - Display the SurveyList component
- /surveys/:id - Display the SurveyDetail component

Add 1 new service

SurveyService

Service should have following methods:

- getSurveys() - HTTP Request - Get a list of surveys (See API Document)
- getSurvey(id) - HTTP Request - Get a single survey from a given survey id